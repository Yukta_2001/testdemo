package coms.LibraryManagement.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import coms.LibraryManagement.entity.LibraryBooks;
import coms.LibraryManagement.repository.BookRepository;
import coms.LibraryManagement.service.LibraryService;

@Service
public class LibraryServiceImpl implements LibraryService{
	
	@Autowired
    BookRepository bookRepo;
	
	@Override
    public List<LibraryBooks> getAllBooks() {
		List<LibraryBooks>  booksAll  =  bookRepo.findAll();
		return booksAll;
    }

    @Override
    public LibraryBooks getBooksById(Long id) {
    	Optional<LibraryBooks> bookid = bookRepo.findById(id);
		if(bookid.isPresent())
			return bookid.get();  // fetching book object
		
		return null;
    }

//    @Override
//    public List<LibraryBooks> getBooksByName(String bookName) {
//        return bookRepo.findByBookName(bookName);
//    }
//
//    @Override
//    public List<LibraryBooks> getBooksByAuthor(String author) {
//        return bookRepo.findByAuthor(author);
//    }
//    
    @Override
    public LibraryBooks getBooksByLastAdded() {
    	List<LibraryBooks> books = bookRepo.findAll();
        return books.get(books.size()-1);

    }

    @Override
    public List<LibraryBooks> getBooksByAscendingOrder() {
    	return bookRepo.findAll(Sort.by("bName"));
    }
  
//    @Override
//    public String addBooks(LibraryBooks book) {
//    	LibraryBooks b = bookRepo.save(book);  // new book
//		if(b!=null)
//			return "Book Added";
//		else
//			return "Fail to add Book";	
//    }
//
    @Override
    public String deleteById(Long bookId) {
    	Optional<LibraryBooks> b = bookRepo.findById(bookId);
		if(b.isPresent())
		{
			bookRepo.deleteById(bookId);
			return "Book Delete"; 
		}
		return "failed to delete the Book";
    }

    @Override
    public void deleteAllBooks() {
        bookRepo.deleteAll();
    }
    
    @Override
    public LibraryBooks addBooks(LibraryBooks book) {
        return bookRepo.save(book);
    }
}
