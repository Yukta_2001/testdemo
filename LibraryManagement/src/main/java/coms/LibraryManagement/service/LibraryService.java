package coms.LibraryManagement.service;

import java.util.List;
import java.util.Optional;

import coms.LibraryManagement.entity.LibraryBooks;

public interface LibraryService {
	
	public List<LibraryBooks> getAllBooks();

    public LibraryBooks getBooksById(Long id);

//    public List<LibraryBooks> getBooksByName(String bookName);
//
//    public List<LibraryBooks> getBooksByAuthor(String author);

    public LibraryBooks getBooksByLastAdded();

    public List<LibraryBooks> getBooksByAscendingOrder();
    
    //  public String addBooks(LibraryBooks book);
    
    public LibraryBooks addBooks(LibraryBooks book);

    public String deleteById(Long bookId);

    public void deleteAllBooks();

}
