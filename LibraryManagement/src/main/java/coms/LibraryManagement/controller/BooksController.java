package coms.LibraryManagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coms.LibraryManagement.entity.LibraryBooks;
import coms.LibraryManagement.serviceimpl.LibraryServiceImpl;

@RestController
//@RequestMapping("/Books")
public class BooksController {
	
	@Autowired
	LibraryServiceImpl libSer;
	
	@PostMapping(value = "/book")
	public ResponseEntity<Object> addLibraryBooks(@RequestBody LibraryBooks books) {
		libSer.addBooks(books);
		libSer.addBooks(books);
		libSer.addBooks(books);
		return new ResponseEntity<>("Books is added successfully", HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/allbooks")
	public ResponseEntity<Object> getBooks() 
 	{
	  List<LibraryBooks>  listall = libSer.getAllBooks();
      return new ResponseEntity(listall, HttpStatus.OK);
    }
	
	 @GetMapping(value = "/book/{bookId}")
	 public ResponseEntity<Object> bookById(@PathVariable Long bookId) {
		   LibraryBooks bid = libSer.getBooksById(bookId);
		   if(bid!=null)
			   return new ResponseEntity(bid, HttpStatus.OK);
		   else
			   return new ResponseEntity("Book Not Found", HttpStatus.OK);			   
	   }
//	 
//	 
//	 @GetMapping(value = "/books/name/{bookName}")
//	    public ResponseEntity<Object> getBookByName(@PathVariable String bookName){
//	        List<LibraryBooks> bname = libSer.getBooksByName(bookName);
//	        if(bname != null)
//	        	return new ResponseEntity(bname, HttpStatus.OK);
//	        else
//	        	return new ResponseEntity("Book name not found", HttpStatus.OK);
//	    }
//
//	    @GetMapping(value = "/books/author/{authorName}")
//	    public ResponseEntity<Object> getBookByAuthor(@PathVariable String author){
//	        List<LibraryBooks> aname = libSer.getBooksByAuthor(author);
//	        if(aname != null)
//	        	return new ResponseEntity(aname, HttpStatus.OK);
//	        else
//	        	return new ResponseEntity("Author name not found", HttpStatus.OK);
//	    }

	    @GetMapping(value = "/booksByAsc")
	    public List<LibraryBooks> getBooksInAscendingOrder(){
	        return  libSer.getBooksByAscendingOrder();
	    }

	    @GetMapping(path = "/booksByLast")
	    public LibraryBooks getLastAddedBook(){
	        return libSer.getBooksByLastAdded();
	    }

	    @DeleteMapping(value = "/deleteById")
	    public ResponseEntity<Object> delete(@PathVariable Long bookId) { 
			   libSer.deleteById(bookId);
			   return new ResponseEntity<>("Book is deleted successsfully", HttpStatus.OK);
		   }

	    @DeleteMapping(value = "/deleteAllBooks")
	    public void deleteAllBooks(){
	        libSer.deleteAllBooks();
	    }

//	 @PostMapping(path = "/book")
//	    public LibraryBooks addBooks(@RequestBody LibraryBooks book){
//	        return libSer.addBooks(book);
//	    }

}
